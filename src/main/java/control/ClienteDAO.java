package control;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Cliente;

/**
 * Classe que ClienteDAO implementa interface OperacoesDAO
 *
 * @author Guilherme
 */
public class ClienteDAO implements InterfaceBanco {

    private static ClienteDAO instance;
    
    private ClienteDAO() {
    }
    
    public static ClienteDAO getInstance() {
        if (instance==null)
            instance = new ClienteDAO();
        return instance;
    }
    
    
    @Override
    public void criar(Connection con, Object obj) throws SQLException {
        if ((obj instanceof Cliente)) {
            Cliente cliente = (Cliente) obj;
            String sql = "insert into CLIENTE ( COD_CLIENTE , NOME_CLIENTE , RG  , CPF , TELEFONE  ,    ENDERECO,      DATA_CADASTRO )  "
                    + "values(s_cliente.nextval, ? ,? ,? ,? ,?  ,sysdate)";

            try (PreparedStatement stm = con.prepareStatement(sql)) {

                stm.setString(1, cliente.getNome());
                stm.setString(2, cliente.getRg());
                stm.setString(3, cliente.getCpf());
                stm.setInt(4, Integer.parseInt(cliente.getTelefone()));
                stm.setString(5, cliente.getEndereco());

                stm.executeUpdate();
                System.out.println("Sucesso");

            } catch (Exception e) {
                // e.printStackTrace(); 

                System.out.println("Inclusão mal sucedida");
            }
        } else {
            System.out.println("Objeto não Cliente");
        }
    }

    @Override
    public List<Object> ler(Connection con) throws SQLException {

        List<Object> lista = new ArrayList<>();
        String sql = "select * from cliente";
        try (PreparedStatement stm = con.prepareStatement(sql);
                ResultSet rs = stm.executeQuery()) {
            while (rs.next()) {
                Cliente c1 = new Cliente(rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6));
                c1.setCodCliente(Long.parseLong(rs.getString(1)));
                lista.add(c1);

            }

        }
        return lista;
    }

    @Override
    public Object pesquisar(Connection con, Object obj) throws SQLException {
        if (obj instanceof Cliente) {
            Cliente cl = (Cliente) obj;

            String sql = "SELECT * FROM CLIENTE WHERE rg =" + cl.getRg();
            try (PreparedStatement stm = con.prepareStatement(sql);
                    ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    Cliente c = new Cliente(rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6));
                    c.setCodCliente(Long.parseLong(rs.getString(1)));
            //        c.setDataCadastro(rs.getString(7));
                    //setCodCliente(Long.parseLong(rs.getString(1)));

                    if (cl.getRg().contains(c.getRg())) {
                        //    JOptionPane.showMessageDialog(null, c);
                        return c;
                    } else {
                        System.out.println("Não Encontrado");
                    }

                }

            }
        }

        return null;

    }

    @Override
    public void alterar(Connection con, Object obj) throws SQLException {
        Object o = pesquisar(con, obj);

        if (o != null) {

            Cliente cl = (Cliente) obj;
            String nome = "'" + cl.getNome() + "'";
            String sql = "UPDATE  CLIENTE SET NOME_CLIENTE='" + cl.getNome()
                    + "', RG=" + cl.getRg()
                    + ", CPF=" + cl.getCpf()
                    + ", TELEFONE='" + cl.getTelefone()
                    + "', ENDERECO='" + cl.getEndereco()
                    + "' WHERE COD_CLIENTE=" + cl.getCodCliente();

            System.out.println(sql);
            try (PreparedStatement stm = con.prepareStatement(sql)) {
                stm.executeUpdate();
                System.out.println("Cliente Alterado com Sucesso");

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Inclusão mal sucedida");
            }
        } else {
            System.out.println("Não Encontrado");
        }
    }

    @Override
    public void excluir(Connection con, Object obj) throws SQLException {
        //Object o = pesquisar(con, obj);
        String std=(String) obj;
        
//        if (o != null) {
//            Cliente cl = (Cliente) obj;
//            String nome = "'" + cl.getNome() + "'";
//            String sql = "DELETE FROM CLIENTE"
//                    + "  WHERE RG=" + cl.getRg();
//
//            try (PreparedStatement stm = con.prepareStatement(sql)) {
//                stm.executeUpdate();
//                System.out.println("Cliente Excluido com Sucesso");
//            } catch (Exception e) {
//                e.printStackTrace();
//                System.out.println("Inclusão mal sucedida");
//            }
//        } else {
//            System.out.println("Não Encontrado");
//        }
        
           if (obj != null) {
            String sql = "DELETE FROM CLIENTE  WHERE Cod_Cliente=" + std;
            try (PreparedStatement stm = con.prepareStatement(sql)) {
                stm.executeUpdate();
                System.out.println("Cliente Excluido com Sucesso");
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Inclusão mal sucedida");
            }
        } else {
            System.out.println("Não Encontrado");
        }
    

    }

//    public void excluir(Connection con, String obj) throws SQLException {
//        if (obj != null) {
//            String sql = "DELETE FROM CLIENTE  WHERE Cod_Cliente=" + obj;
//            try (PreparedStatement stm = con.prepareStatement(sql)) {
//                stm.executeUpdate();
//                System.out.println("Cliente Excluido com Sucesso");
//            } catch (Exception e) {
//                e.printStackTrace();
//                System.out.println("Inclusão mal sucedida");
//            }
//        } else {
//            System.out.println("Não Encontrado");
//        }
//    }

}
