package control;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Karen
 */
public interface  Comando {
    
    /**
     *
     * @param request
     * @param response
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws IOException
     * @throws ServletException
     */
    public abstract void executar(HttpServletRequest request, HttpServletResponse response) 
            throws ClassNotFoundException, SQLException, IOException, ServletException ;
   
}
