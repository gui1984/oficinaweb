package control;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author 0230090
 */
public interface InterfaceBanco {

    public void criar(Connection con, Object obj) throws SQLException;

    public List<Object> ler(Connection con) throws SQLException;

    public void alterar(Connection con, Object conta) throws SQLException;

    public Object pesquisar(Connection con, Object obj) throws SQLException;

    public void excluir(Connection con, Object conta) throws SQLException;

}
