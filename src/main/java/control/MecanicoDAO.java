package control;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JOptionPane;
import model.Mecanico;

/**
 * Classe que MecanicoDAO implementa interface OperacoesDAO
 *
 * @author 0230090
 */
public class MecanicoDAO implements InterfaceBanco {

    @Override
    public void criar(Connection con, Object obj) throws SQLException {
        if ((obj instanceof Mecanico)) {
            Mecanico mec = (Mecanico) obj;
            String sql = "insert into   MECANICO     (  COD_MECANICO  ,  NOME_MECANICO     )  "
                    + "   values(s_mecanico.nextval, ? )";

            try (PreparedStatement stm = con.prepareStatement(sql)) {

                stm.setString(1, mec.getNomeMec());

                stm.executeUpdate();
                System.out.println("Sucesso");

            } catch (Exception e) {
                // e.printStackTrace(); 

                System.out.println("Inclusão mal sucedida");
            }
        } else {
            System.out.println("Objeto não Cliente");
        }

    }

    @Override
    public List<Object> ler(Connection con) throws SQLException {
        List<Object> lista = new ArrayList<>();
        String sql = "select * from mecanico";
        try (PreparedStatement stm = con.prepareStatement(sql);
                ResultSet rs = stm.executeQuery()) {
            while (rs.next()) {
                Mecanico mec = new Mecanico(rs.getString(2));
                mec.setCodMec(rs.getLong(1));

                lista.add(mec);

            }

        }
        return lista;

    }

    @Override
    public void alterar(Connection con, Object obj) throws SQLException {
        Object o = pesquisar(con, obj);
        //System.out.println(o);
        if (o != null) {
            
            Mecanico mec = (Mecanico) o;
            //String nome = "'" + mec.getNomeMec() + "'";
            String novoNome = JOptionPane.showInputDialog(null, "Informe o novo nome do Mecanico");
            String sql = "UPDATE  Mecanico SET NOME_MECANICO ='" + novoNome + "'  WHERE COD_MECANICO=" + mec.getCodMec();
            System.out.println(sql);
            try (PreparedStatement stm = con.prepareStatement(sql)) {
                stm.executeUpdate();
                JOptionPane.showMessageDialog(null, "Mecanico Alterado com Sucesso");

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Inclusão mal sucedida");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Não Encontrado");
        }

    }

    @Override
    public Object pesquisar(Connection con, Object obj) throws SQLException {
         if (obj instanceof Mecanico) {
            Mecanico mec = (Mecanico) obj;

            String sql = "SELECT * FROM Mecanico WHERE NOME_MECANICO=" + "'"+mec.getNomeMec()+"'";
            System.out.println(sql);
            try (PreparedStatement stm = con.prepareStatement(sql);
                    ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    Mecanico mec1 = new Mecanico(rs.getString(2));
                    mec1.setCodMec(rs.getLong(1));

                    //(c.getPlaca().contains(c1.getPlaca()))
                    if (mec1.getNomeMec().contains( mec.getNomeMec() )
                            ) {
                        //    JOptionPane.showMessageDialog(null, c);
                        return mec1;
                    } else {
                        JOptionPane.showMessageDialog(null, "Não Encontrado");
                    }

                }

            }
        }

        return null;

    }

    @Override
    public void excluir(Connection con, Object obj) throws SQLException {
         Object o = pesquisar(con, obj);
        //System.out.println(o);
        if (o != null) {
            
            Mecanico mec = (Mecanico) o;
            //String nome = "'" + mec.getNomeMec() + "'";
            //String novoNome = JOptionPane.showInputDialog(null, "Informe o novo nome do Mecanico");
            //String sql = "DELETE FROM CLIENTE"                    + "  WHERE RG=" + cl.getRg();
            String sql = "DELETE FROM  Mecanico   WHERE COD_MECANICO=" + mec.getCodMec();
            System.out.println(sql);
            try (PreparedStatement stm = con.prepareStatement(sql)) {
                stm.executeUpdate();
                JOptionPane.showMessageDialog(null, "Mecanico Alterado com Sucesso");

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Inclusão mal sucedida");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Não Encontrado");
        }
    }

}
