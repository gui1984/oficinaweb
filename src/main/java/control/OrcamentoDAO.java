package control;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import javax.swing.JOptionPane;
import model.Orcamento;
import model.Peca;
import model.Servico;

/**
 * Classe que OrcamentoDAO implementa interface OperacoesDAO
 *
 * @author 0230090
 */
public class OrcamentoDAO implements InterfaceBanco {

    @Override
    public void criar(Connection con, Object obj) throws SQLException {
        if ((obj instanceof Orcamento)) {
            Orcamento orc = (Orcamento) obj;
            String sql
                    = /*
                     "insert into  ORCAMENTO ( COD_ORCAMENTO ,  Cod_veiculo  , 	Data_Entrada , defeito	 )    "
                     + "values(s_orcamento.nextval, ?  ,sysdate,?)";*/ "call easyOrc(?,?)";

            try (PreparedStatement stm = con.prepareStatement(sql)) {

                stm.setLong(1, orc.getCodVeiculo());
                stm.setString(2, orc.getDefeito());

                stm.executeUpdate();
                System.out.println("Sucesso");

            } catch (Exception e) {
                e.printStackTrace();

                System.out.println(sql);
                System.out.println("Inclusão mal sucedida");
            }
        } else {
            System.out.println("Objeto não Cliente");
        }

    }

    @Override
    public List<Object> ler(Connection con) throws SQLException {
        List<Object> lista = new ArrayList<>();
        String sql = "select * from ORCAMENTO";
        try (PreparedStatement stm = con.prepareStatement(sql);
                ResultSet rs = stm.executeQuery()) {
            while (rs.next()) {
                Orcamento ss = new Orcamento(rs.getLong(2));
                //Long.parseLong(rs.getString(1))
                //ss.setCodVeiculo(Long.parseLong(rs.getString(2)));
                // ss.setCodVeiculo(rs.getLong(2));

                // ss.setAprovado(Boolean.parseBoolean(rs.getString(6)));
                ss.setCodMecanico(rs.getLong(3));
                ss.setCodOrcamento(rs.getLong(1));
                ss.setDataEntrada(rs.getDate(5));
                ss.setDataEntrega(rs.getDate(7));
                ss.setDefeito(rs.getString(4));
                ss.setValor(rs.getDouble(8));

                if (rs.getString(6).contains("true")) {

                    ss.setAprovado(true);
                } else {
                    ss.setAprovado(false);
                }

                {

                    String sql1 = "select Orc_Peca.Cod_Orcamento,Orc_Peca.Cod_Peca,"
                            + "Peca.Nome_Peca, Orc_Peca.Quantidade_Usada,Peca.Valor_Peca "
                            + "from ORC_PECA "
                            + "inner join Peca "
                            + "on Peca.Cod_Peca =Orc_Peca.Cod_Peca "
                            + "WHERE COD_ORCAMENTO=" + "'" + ss.getCodOrcamento() + "'";
                    try (PreparedStatement stm1 = con.prepareStatement(sql1);
                            ResultSet rs1 = stm1.executeQuery()) {
                        while (rs1.next()) {
                            /*
                             System.out.println("\nV ");
                             System.out.println(rs1.getString(1));
                             System.out.println(rs1.getString(2));
                             System.out.println(rs1.getString(3));
                             System.out.println(rs1.getString(4));
                             System.out.println(rs1.getString(5));
                             System.out.println("");
                             */
                            Peca pp = new Peca(rs1.getString(3), rs1.getDouble(5));
                            pp.setQuantidade(rs1.getInt(4));
                            pp.setCodPeca(Long.parseLong(rs1.getString(2)));
                            ss.setPecas(pp);

                        }
                    }

                    {
                        String sql2 = "select Orc_Servico.Cod_Orcamento, Orc_Servico.Cod_Servico, "
                                + " Servico.Nome_Servico,Servico.Descricao, Servico.Valor_Servico "
                                + "from ORC_SERVICO "
                                + "inner Join Servico "
                                + "on Servico.Cod_Servico=Orc_Servico.Cod_Servico "
                                + "WHERE COD_ORCAMENTO=" + "'" + ss.getCodOrcamento() + "'";
                        try (PreparedStatement stm2 = con.prepareStatement(sql2);
                                ResultSet rs2 = stm2.executeQuery()) {
                            while (rs2.next()) {
                                /*
                                 System.out.println("S ");
                                 System.out.println(rs2.getString(1));
                                 System.out.println(rs2.getString(2));
                                 System.out.println(rs2.getString(3));
                                 System.out.println(rs2.getString(4));
                                 System.out.println(rs2.getString(5));
                                 System.out.println("");
                                 */
                                Servico so;
                                so = new Servico(rs2.getString(3), rs2.getString(4), Double.parseDouble(rs2.getString(5)));
                                so.setCodServico(Long.parseLong(rs2.getString(2)));
                                //  System.out.println(so);
                                ss.setServico(so);
                            }
                        }
                    }
                }

                lista.add(ss);
            }

        }
        return lista;

    }

    @Override
    public Object pesquisar(Connection con, Object obj) throws SQLException {

        if (obj instanceof Orcamento) {
            Orcamento ss = (Orcamento) obj;

            String sql = "SELECT * FROM Orcamento WHERE COD_ORCAMENTO=" + "'" + ss.getCodOrcamento() + "'";
            System.out.println(sql);
            try (PreparedStatement stm = con.prepareStatement(sql);
                    ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    Orcamento ss1 = new Orcamento(rs.getLong(2));
                    ss1.setCodOrcamento(rs.getLong(1));
                    ss.setCodMecanico(rs.getLong(3));
                    ss.setDataEntrada(rs.getDate(5));
                    ss.setDataEntrega(rs.getDate(7));
                    ss.setDefeito(rs.getString(4));
                    ss.setValor(rs.getDouble(8));

                    if (rs.getString(6).contains("true")) {

                        ss.setAprovado(true);
                    } else {
                        ss.setAprovado(false);
                    }

                    {

                        String sql1 = "select Orc_Peca.Cod_Orcamento,Orc_Peca.Cod_Peca,"
                                + "Peca.Nome_Peca, Orc_Peca.Quantidade_Usada,Peca.Valor_Peca "
                                + "from ORC_PECA "
                                + "inner join Peca "
                                + "on Peca.Cod_Peca =Orc_Peca.Cod_Peca "
                                + "WHERE COD_ORCAMENTO=" + "'" + ss.getCodOrcamento() + "'";
                        try (PreparedStatement stm1 = con.prepareStatement(sql1);
                                ResultSet rs1 = stm1.executeQuery()) {
                            while (rs1.next()) {
                                /*
                                 System.out.println("\nV ");
                                 System.out.println(rs1.getString(1));
                                 System.out.println(rs1.getString(2));
                                 System.out.println(rs1.getString(3));
                                 System.out.println(rs1.getString(4));
                                 System.out.println(rs1.getString(5));
                                 System.out.println("");
                                 */
                                Peca pp = new Peca(rs1.getString(3), rs1.getDouble(5));
                                pp.setQuantidade(rs1.getInt(4));
                                pp.setCodPeca(Long.parseLong(rs1.getString(2)));
                                ss.setPecas(pp);

                            }
                        }

                        {
                            String sql2 = "select Orc_Servico.Cod_Orcamento, Orc_Servico.Cod_Servico, "
                                    + " Servico.Nome_Servico,Servico.Descricao, Servico.Valor_Servico "
                                    + "from ORC_SERVICO "
                                    + "inner Join Servico "
                                    + "on Servico.Cod_Servico=Orc_Servico.Cod_Servico "
                                    + "WHERE COD_ORCAMENTO=" + "'" + ss.getCodOrcamento() + "'";
                            try (PreparedStatement stm2 = con.prepareStatement(sql2);
                                    ResultSet rs2 = stm2.executeQuery()) {
                                while (rs2.next()) {
                                    /*
                                     System.out.println("S ");
                                     System.out.println(rs2.getString(1));
                                     System.out.println(rs2.getString(2));
                                     System.out.println(rs2.getString(3));
                                     System.out.println(rs2.getString(4));
                                     System.out.println(rs2.getString(5));
                                     System.out.println("");
                                     */
                                    Servico so;
                                    so = new Servico(rs2.getString(3), rs2.getString(4),
                                            Double.parseDouble(rs2.getString(5)));
                                    so.setCodServico(Long.parseLong(rs2.getString(2)));
                                    //  System.out.println(so);
                                    ss.setServico(so);
                                }
                            }
                        }
                    }

                    //(c.getPlaca().contains(c1.getPlaca()))
                    if (ss1.getCodOrcamento() == ss.getCodOrcamento()) {
                        //    JOptionPane.showMessageDialog(null, c);
                        return ss;
                    } else {
                        JOptionPane.showMessageDialog(null, "Não Encontrado");
                    }

                }

            }
        }

        return null;
    }

    @Override
    public void alterar(Connection con, Object obj) throws SQLException {
        // System.out.println("z10 " + obj);
        //Object o = pesquisar(con, obj);
        //System.out.println(o);
        if (obj != null) {
            Orcamento ss = (Orcamento) obj;

            //String nome = "'" + mec.getNomeMec() + "'";
            //  System.out.println(ss);
            // String newstring = new SimpleDateFormat("dd/mm/yyyy").format(ss.getDataEntrega());
      

            String sql
                    = /*
                     "update Orcamento "
                     + "set Aprovado='" + ss.isAprovado()
                     + "',Cod_Mecanico=" + ss.getCodMecanico()
                     + ",Valor_Total=" + ss.getValorTotal()
                     + ",Defeito='" + ss.getDefeito()
                     + "',Data_Entrega=" + "sysdate"
                     // "to_date('"+newstring+"', 'dd/mm/yyyy ')"
                     + " Where Cod_Orcamento=" + ss.getCodOrcamento(); 
                     */ 
                    "call proc_alt_Orcamento('" + ss.isAprovado() + "',"
                    + ss.getCodMecanico() + ","
                    + ss.getValorTotal() + ",'"
                    + ss.getDefeito() + "',"
                    + ss.getCodOrcamento() + ")";

            System.out.println(sql);
            try (PreparedStatement stm = con.prepareStatement(sql)) {

//                stm.setBoolean(1, ss.isAprovado());
//                stm.setLong(2, ss.getCodMecanico());
//                stm.setDouble(3, ss.getValorTotal());
//                stm.setString(4, ss.getDefeito());
//                stm.setLong(5, ss.getCodOrcamento());
                stm.executeUpdate();
                System.out.println(stm);
                JOptionPane.showMessageDialog(null, "Orcamento Alterado com Sucesso");

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(sql);
                System.out.println("Inclusão mal sucedida");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Não Encontrado");
        }
    }

    public void alterarOP(Connection con, Object obj) throws SQLException {

        if (obj != null) {
            Orcamento ss = (Orcamento) obj;
            Set<Peca> pecasUsadas = ss.getPecasUsadas();

            String sql3 = "";
            for (Peca peca : pecasUsadas) {
//                        System.out.println("CodOrcamento="+ss.getCodOrcamento() + " ");
//                        System.out.println(peca);

                sql3
                        = /*
                         "insert into  ORC_PECA   "
                         + "   (Cod_Orcamento , QUANTIDADE_USADA  ,  Cod_Peca  )"
                         + "    values(  ?  , ? ,?) ";*/ "call easyOrcPeca(?,?,?)";
                try (PreparedStatement stm4 = con.prepareStatement(sql3)) {

                    stm4.setLong(1, ss.getCodOrcamento());
                    stm4.setLong(2, peca.getQuantidade());
                    stm4.setLong(3, peca.getCodPeca());

//                            System.out.println(sql3);
//                            System.out.println(stm4.getParameterMetaData());
                    stm4.executeUpdate();
                    System.out.println("Sucesso");
                } catch (java.sql.SQLIntegrityConstraintViolationException eo) {
                    // eo.printStackTrace();
                    System.out.println("Chave Existente");

                } catch (Exception e) {
                    // e.printStackTrace(); 

                    System.out.println("Inclusão mal sucedida");
                }

            }
        } else {
            JOptionPane.showMessageDialog(null, "Não Encontrado");
        }
    }

    public void alterarOS(Connection con, Object obj) throws SQLException {

        if (obj != null) {
            Orcamento ss = (Orcamento) obj;
            Set<Servico> servicoPrestados = ss.getServicoPrestados();

            String sql3 = "";

            for (Servico servico : servicoPrestados) {
//                        System.out.println("CodOrcamento="+ss.getCodOrcamento() + " ");
//                        System.out.println(servico);
                sql3
                        = /*
                         "insert into  ORC_SERVICO  "
                         + "   (Cod_Orcamento , Cod_servico 	 )"
                         + "    values(  ?  , ? ) ";*/ "call easyOrcSERV(?,?)";
                try (PreparedStatement stm4 = con.prepareStatement(sql3)) {

                    stm4.setLong(1, ss.getCodOrcamento());
                    stm4.setLong(2, servico.getCodServico());

//                            System.out.println(sql3);
//                            System.out.println(stm4.getParameterMetaData());
                    stm4.executeUpdate();
                    System.out.println("Sucesso");
                } catch (java.sql.SQLIntegrityConstraintViolationException eo) {
                    // eo.printStackTrace();
                    System.out.println("Chave Existente");

                } catch (Exception e) {
                    //e.printStackTrace(); 

                    System.out.println("Inclusão mal sucedida");
                }
            }

        } else {
            JOptionPane.showMessageDialog(null, "Não Encontrado");
        }
    }
    /*
     @Override
     public void alterar(Connection con, Object obj) throws SQLException {

     Object o = pesquisar(con, obj);
     //System.out.println(o);
     if (o != null) {

     Orcamento ss = (Orcamento) o;
     //String nome = "'" + mec.getNomeMec() + "'";
     String sql;
     int resultado = JOptionPane.showConfirmDialog(null, "Aprovar Orçamento?");
     if (resultado == 0) {
     sql = "UPDATE  Orcamento SET Aprovado  ='" + "true" + "'  WHERE COD_Orcamento=" + ss.getCodOrcamento();
     } else {
     sql = "UPDATE  Orcamento SET Aprovado  ='" + "false" + "'  WHERE COD_Orcamento=" + ss.getCodOrcamento();
     }

     System.out.println(sql);
     try (PreparedStatement stm = con.prepareStatement(sql)) {
     stm.executeUpdate();
     JOptionPane.showMessageDialog(null, "Orcamento Alterado com Sucesso");

     } catch (Exception e) {
     e.printStackTrace();
     System.out.println("Inclusão mal sucedida");
     }
     } else {
     JOptionPane.showMessageDialog(null, "Não Encontrado");
     }

     }
     */

    @Override
    public void excluir(Connection con, Object obj) throws SQLException {

        Object o = pesquisar(con, obj);

        if (o != null) {

            Orcamento ss = (Orcamento) o;

            String sql = //"DELETE FROM  Orcamento    WHERE COD_Orcamento=" + ss.getCodOrcamento();
                    "call proc_del_orc(?)";
            System.out.println(sql);
            try (PreparedStatement stm = con.prepareStatement(sql)) {
                stm.setLong(1, ss.getCodOrcamento());
                stm.executeUpdate();
                JOptionPane.showMessageDialog(null, "Orcamento Alterado com Sucesso");

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Inclusão mal sucedida");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Não Encontrado");
        }
    }

}
