package control;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JOptionPane;
import model.Peca;

/**
 * Classe que PecaDAO implementa interface OperacoesDAO
 *
 * @author 0230090
 */
public class PecaDAO implements InterfaceBanco {

    @Override
    public void criar(Connection con, Object obj) throws SQLException {
       
        if ((obj instanceof Peca)) {
            Peca pp = (Peca) obj;
            String sql = "insert into PECA    (   COD_PECA  , 	 NOME_PECA  ,  QUANTIDADE  , 	 VALOR_PECA      )    " +
                    "    values(s_peca.nextval, ? ,? , ?)"                   
                    ;

            try (PreparedStatement stm = con.prepareStatement(sql)) {

                stm.setString(1, pp.getNomePeca());
                stm.setLong(2, pp.getQuantidade());
                stm.setDouble(3, pp.getValor());
                

                stm.executeUpdate();
                System.out.println("Sucesso");

            } catch (Exception e) {
                // e.printStackTrace(); 

                System.out.println("Inclusão mal sucedida");
            }
        } else {
            System.out.println("Objeto não Cliente");
        }
    }

    @Override
    public List<Object> ler(Connection con) throws SQLException {
       
         List<Object> lista = new ArrayList<>();
        String sql = "select * from peca";
        try (PreparedStatement stm = con.prepareStatement(sql);
                ResultSet rs = stm.executeQuery()) {
            while (rs.next()) {
                Peca pp = new Peca(rs.getString(2), rs.getDouble(4));
                pp.setQuantidade(rs.getInt(3));
                pp.setCodPeca(Long.parseLong(rs.getString(1)));
                lista.add(pp);

            }

        }
        return lista;
    }

    @Override
    public void alterar(Connection con, Object obj) throws SQLException {
        Object o = pesquisar(con, obj);
        //System.out.println(o);
        if (o != null) {
            
            Peca pp  = (Peca) o;
            //String nome = "'" + mec.getNomeMec() + "'";
            String novoNome = JOptionPane.showInputDialog(null, "Informe o novo nome da Peca");
            String sql = "UPDATE  Peca SET NOME_PECA  ='" + novoNome + "'  WHERE COD_PECA=" + pp.getCodPeca();
            System.out.println(sql);
            try (PreparedStatement stm = con.prepareStatement(sql)) {
                stm.executeUpdate();
                JOptionPane.showMessageDialog(null, "Peca Alterado com Sucesso");

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Inclusão mal sucedida");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Não Encontrado");
        }

    }

    @Override
    public Object pesquisar(Connection con, Object obj) throws SQLException {
       
         if (obj instanceof Peca) {
            Peca pp = (Peca) obj;

            String sql = "SELECT * FROM PECA WHERE NOME_PECA=" + "'"+pp.getNomePeca()+"'";
            System.out.println(sql);
            try (PreparedStatement stm = con.prepareStatement(sql);
                    ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    Peca pp1 =  new Peca(rs.getString(2), rs.getDouble(4));
                    pp1.setQuantidade(rs.getInt(3));
                    pp1.setCodPeca(rs.getLong(1));

                    //(c.getPlaca().contains(c1.getPlaca()))
                    if (pp1.getNomePeca().contains( pp.getNomePeca()) 
                            ) {
                        //    JOptionPane.showMessageDialog(null, c);
                        return pp1;
                    } else {
                        JOptionPane.showMessageDialog(null, "Não Encontrado");
                    }

                }

            }
        }

        return null;
    }

    @Override
    public void excluir(Connection con, Object obj) throws SQLException {
        Object o = pesquisar(con, obj);
        //System.out.println(o);
        if (o != null) {
            
            Peca pp = (Peca ) o;
            //String nome = "'" + mec.getNomeMec() + "'";
            //String novoNome = JOptionPane.showInputDialog(null, "Informe o novo nome do Mecanico");
            //String sql = "DELETE FROM CLIENTE"                    + "  WHERE RG=" + cl.getRg();
            String sql = "DELETE FROM  Peca    WHERE COD_PECA=" + pp.getCodPeca();
            System.out.println(sql);
            try (PreparedStatement stm = con.prepareStatement(sql)) {
                stm.executeUpdate();
                JOptionPane.showMessageDialog(null, "Peca Alterado com Sucesso");

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Inclusão mal sucedida");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Não Encontrado");
        }
    }

    
}
