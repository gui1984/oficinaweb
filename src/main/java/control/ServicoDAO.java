package control;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JOptionPane;
import model.Peca;
import model.Servico;

/**
 * Classe que ServicoDAO implementa interface OperacoesDAO
 *
 * @author 0230090
 */
public class ServicoDAO implements InterfaceBanco {

   
    @Override
    public void criar(Connection con, Object obj) throws SQLException {
       
        if ((obj instanceof Servico )) {
            Servico ss = (Servico) obj;
            String sql = "insert into  SERVICO   ( COD_SERVICO  , NOME_SERVICO  , DESCRICAO  ,   VALOR_SERVICO)   " +
                    "    values(s_servico, ? ,? , ?)"                   
                    ;

            try (PreparedStatement stm = con.prepareStatement(sql)) {

                stm.setString(1, ss.getNomeServico());
                stm.setString(2, ss.getDescricao());
                stm.setDouble(3, ss.getValorServico());
                

                stm.executeUpdate();
                System.out.println("Sucesso");

            } catch (Exception e) {
                // e.printStackTrace(); 

                System.out.println("Inclusão mal sucedida");
            }
        } else {
            System.out.println("Objeto não Cliente");
        }
    }

    @Override
    public List<Object> ler(Connection con) throws SQLException {
       
         List<Object> lista = new ArrayList<>();
        String sql = "select * from servico";
        try (PreparedStatement stm = con.prepareStatement(sql);
                ResultSet rs = stm.executeQuery()) {
            while (rs.next()) {
                Servico  ss = new Servico (rs.getString(2), rs.getString(3),rs.getDouble(4));
                ss.setCodServico(Long.parseLong(rs.getString(1)));
                
                lista.add(ss);

            }

        }
        return lista;
    }

    @Override
    public void alterar(Connection con, Object obj) throws SQLException {
        Object o = pesquisar(con, obj);
        //System.out.println(o);
        if (o != null) {
            
            Servico ss = (Servico) o;
            //String nome = "'" + mec.getNomeMec() + "'";
            String novoNome = JOptionPane.showInputDialog(null, "Informe o novo nome do Servico");
            String sql = "UPDATE  SERVICO SET NOME_SERVICO  ='" + novoNome + "'  WHERE COD_SERVICO=" + ss.getCodServico();
            System.out.println(sql);
            try (PreparedStatement stm = con.prepareStatement(sql)) {
                stm.executeUpdate();
                JOptionPane.showMessageDialog(null, "Servico Alterado com Sucesso");

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Inclusão mal sucedida");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Não Encontrado");
        }

    }

    @Override
    public Object pesquisar(Connection con, Object obj) throws SQLException {
       
         if (obj instanceof Servico) {
            Servico ss = (Servico) obj;

            String sql = "SELECT * FROM Servico WHERE NOME_Servico=" + "'"+ss.getNomeServico()+"'";
            System.out.println(sql);
            try (PreparedStatement stm = con.prepareStatement(sql);
                    ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    Servico ss1 = new Servico (rs.getString(2), rs.getString(3),rs.getDouble(4));
                    ss1.setCodServico(rs.getLong(1));
                    

                    //(c.getPlaca().contains(c1.getPlaca()))
                    if (ss1.getNomeServico().contains( ss.getNomeServico()) 
                            ) {
                        //    JOptionPane.showMessageDialog(null, c);
                        return ss1;
                    } else {
                        JOptionPane.showMessageDialog(null, "Não Encontrado");
                    }

                }

            }
        }

        return null;
    }

    @Override
    public void excluir(Connection con, Object obj) throws SQLException {
        Object o = pesquisar(con, obj);
        //System.out.println(o);
        if (o != null) {
            
                        Servico ss = (Servico) o;
            //String nome = "'" + mec.getNomeMec() + "'";
            //String novoNome = JOptionPane.showInputDialog(null, "Informe o novo nome do Mecanico");
            //String sql = "DELETE FROM CLIENTE"                    + "  WHERE RG=" + cl.getRg();
            String sql = "DELETE FROM  Servico    WHERE COD_Servico=" + ss.getCodServico();
            System.out.println(sql);
            try (PreparedStatement stm = con.prepareStatement(sql)) {
                stm.executeUpdate();
                JOptionPane.showMessageDialog(null, "Servico Alterado com Sucesso");

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Inclusão mal sucedida");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Não Encontrado");
        }
    }

    
  
}
