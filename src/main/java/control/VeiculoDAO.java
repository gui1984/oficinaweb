package control;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JOptionPane;
import model.Cliente;
import model.Veiculo;

/**
 * Classe que VeiculoDAO implementa interface OperacoesDAO
 *
 * @author 0230090
 */
public class VeiculoDAO implements InterfaceBanco {

    @Override
    public void criar(Connection con, Object obj) throws SQLException {
        if (obj instanceof Veiculo) {
            Veiculo cl = (Veiculo) obj;

            String sql = "insert into   VEICULO  (  COD_VEICULO  ,  PLACA    ,    CHASSI  ,   COR  ,    PARTICULARIDADE  ,  COD_CLIENTE   )"
                    + "   values(s_veiculo.nextval, ?, ? , ?, ? , ?)";

            try (PreparedStatement stm = con.prepareStatement(sql)) {

                stm.setString(1, cl.getPlaca());
                stm.setString(2, cl.getChassi());
                stm.setString(3, cl.getCor());
                stm.setString(4, cl.getParticularidades());
                stm.setLong(5, cl.getIdCliente());

                stm.executeUpdate();
                System.out.println("Sucesso");

            } catch (Exception e) {
                // e.printStackTrace(); 

                System.out.println("Inclusão mal sucedida");
            }

        } else {
            System.out.println("Objeto não Veículo");
        }
    }

    @Override
    public List<Object> ler(Connection con) throws SQLException {
        List<Object> lista = new ArrayList<>();
        String sql = "select * from veiculo";
        try (PreparedStatement stm = con.prepareStatement(sql);
                ResultSet rs = stm.executeQuery()) {
            while (rs.next()) {
                Veiculo c1 = new Veiculo(rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
                c1.setCodVEiculo(rs.getLong(1));
                //        setId(Long.parseLong(rs.getString(1)));
                long id = rs.getLong(6);
                c1.setIdCliente(id);

                lista.add(c1);

            }

        }
        return lista;

    }

    @Override
    public void alterar(Connection con, Object obj) throws SQLException {
        Object o = pesquisar(con, obj);

        if (o != null) {

            Veiculo cl = (Veiculo) obj;
            String nome = JOptionPane.showInputDialog(null, "Informe a nova Cor do Veiculo");
            //String nome = "'" + cl.getChassi() + "'";
            
            String sql = "UPDATE  VEICULO SET COR=" + "'"+nome +"'"+ "  WHERE PLACA=" + "'"+cl.getPlaca()+"'";

            try (PreparedStatement stm = con.prepareStatement(sql)) {
                stm.executeUpdate();
                JOptionPane.showMessageDialog(null, "VEICULO Alterado com Sucesso");

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Inclusão mal sucedida");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Não Encontrado");
        }

    }

    @Override
    public Object pesquisar(Connection con, Object obj) throws SQLException {
        if (obj instanceof Veiculo) {
            Veiculo c = (Veiculo) obj;
            String pl="'"+c.getPlaca()+"'";
            String sql = "SELECT * FROM VEICULO WHERE PLACA=" + pl;
            System.out.println(sql);
            
            try (PreparedStatement stm = con.prepareStatement(sql); ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    Veiculo c1 = new Veiculo(rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
                    c1.setCodVEiculo(rs.getLong(1));
                    //        setId(Long.parseLong(rs.getString(1)));
                    long id = rs.getLong(6);
                    c1.setIdCliente(id);
                    
                    if (c.getPlaca().contains(c1.getPlaca())) {
                        //    JOptionPane.showMessageDialog(null, c);
                        return c1;
                    } else {
                        JOptionPane.showMessageDialog(null, "Não Encontrado");
                    }

                }
            }
        }
        return null;

    }

    @Override
    public void excluir(Connection con, Object obj) throws SQLException {
        System.out.println("Entrou");
        Object o = pesquisar(con, obj);
        if (o != null) {
            Veiculo cl = (Veiculo) obj;

            // String nome = "'" + cl.getChassi() + "'";
            String sql = "DELETE FROM VEICULO   WHERE PLACA=" + "'"+cl.getPlaca()+"'";

            try (PreparedStatement stm = con.prepareStatement(sql)) {
                stm.executeUpdate();
                JOptionPane.showMessageDialog(null, "VEICULO deletado com Sucesso");

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Inclusão mal sucedida");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Não Encontrado");
        }

    }

}
