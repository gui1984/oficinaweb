package model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *Essa classe define o cliente com seus atributos e metodos proprios
 * @author Guilherme  e Susane
 */
public class Cliente implements Serializable { 
    
    /**
     * atributo da interface serializable
     */
    private static final long serialVersionUID = 7568806323650708505L;
    /**
     * Atributos Privados da Classe Cliente
     */	
    private long codCliente; 
    private String nome; 
    private String rg; 
    private String cpf; 
    private String telefone; 
    private String endereco; 
    private Date dataCadastro; 

    /**
     * Método contrutor para usar em consultas DAO
     * @param rg String
     */
    public Cliente(String rg) {
        this.rg = rg;
    }
      
    /**
     *Metodos construtor da classe
     * completos com todos os atributos como parametro,
     * atributo data(hoje) pego automaticamento do sistema
     * @param nome String
     * @param rg String
     * @param cpf String
     * @param telefone String
     * @param endereco String
     */
    public Cliente(String nome, String rg, String cpf, String telefone, String endereco) { 
        this.nome = nome; 
        this.rg = rg; 
        this.cpf = cpf; 
        this.telefone = telefone; 
        this.endereco = endereco; 
        this.dataCadastro= new Date(System.currentTimeMillis());
    } 
  
    /**
     *Metodos construtor da classe,
     * faltando apenas o endereço que usará o valor padrao,
     * atributo data(hoje) pego automaticamento do sistema
     * @param nome String
     * @param rg String
     * @param cpf String
     * @param telefone String
     */
    public Cliente(String nome, String rg, String cpf, String telefone) { 
        this.nome = nome; 
        this.rg = rg; 
        this.cpf = cpf; 
        this.telefone = telefone; 
        this.endereco = "Porto Alegre"; 
        this.dataCadastro= new Date(System.currentTimeMillis());
        
    } 
    
    /**
     * Outro método consrutor
     * para receber um objeto cliente
     * @param cl um cliente
     
     public Cliente(Cliente cl){
         this.codCliente = cl.getCodCliente();
         this.dataCadastro= cl.getDataCadastro();
         this.nome = cl.getNome(); 
        this.rg = cl.getRg();
        this.cpf = cl.getCpf(); 
        this.telefone = cl.getTelefone(); 
        this.endereco = cl.getEndereco();                       
     }
     */
    
     /**
     * Métodos Setter e Getter
     */

    /**
     *
     * @return String nome
     */
    public String getNome() { 
        return nome; 
    } 
  
    /**
     *
     * @param nome String
     */
    public void setNome(String nome) { 
        this.nome = nome; 
    } 
  
    /**
     *
     * @return String rg
     */
    public String getRg() { 
        return rg; 
    } 
  
    /**
     *
     * @param rg String
     */
    public void setRg(String rg) { 
        this.rg = rg; 
    } 
  
    /**
     *
     * @return String cpf
     */
    public String getCpf() { 
        return cpf; 
    } 
  
    /**
     *
     * @param cpf String
     */
    public void setCpf(String cpf) { 
        this.cpf = cpf; 
    } 
  
    /**
     *
     * @return String  telefone
     */
    public String getTelefone() { 
        return telefone; 
    } 
  
    /**
     *
     * @param telefone String
     */
    public void setTelefone(String telefone) { 
        this.telefone = telefone; 
    } 
  
    /**
     *
     * @return String endereco
     */
    public String getEndereco() { 
        return endereco; 
    } 
  
    /**
     *
     * @param endereco String
     */
    public void setEndereco(String endereco) { 
        this.endereco = endereco; 
    } 
  
    /**
     *
     * @return dataCadastro Date
     */
    public Date getDataCadastro() { 
        return dataCadastro; 
    } 

    public void setDataCadastro(String dataCadastro) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date data = formatter.parse(dataCadastro);
            this.dataCadastro = data;
        } catch (ParseException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    /**
     *
     * @return codCliente long
     */ 
    public long getCodCliente() { 
        return codCliente; 
    } 
  
    /**
     *
     * @param CodCliente long
     */
    public void setCodCliente(long CodCliente) { 
        this.codCliente = CodCliente; 
    } 

    /**
     *Hashcode gerado automaticamente
     * @return int
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.nome);
        hash = 97 * hash + Objects.hashCode(this.rg);
        hash = 97 * hash + Objects.hashCode(this.cpf);
        return hash;
    }

    /**
     *Equals gerado automaticamente
     * @param obj Object Cliente
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cliente other = (Cliente) obj;
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        if (!Objects.equals(this.rg, other.rg)) {
            return false;
        }
        return Objects.equals(this.cpf, other.cpf);
    }
  
    /**Sobrescreve a classe objeto 
     *toString retornando uma  String com todos os atributos
     * @return String
     */
    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); //formata a data para visualização certa
        String dataC=" ";
        if (dataCadastro!=null){
         dataC=sdf.format( dataCadastro );
        } 
        return "Cliente{" + "\n RG=" + rg +
                ",\nCod_Cliente=" + codCliente + 
                ",\n nome=" + nome 
                +  ",\n cpf="  + cpf 
                + ",\n telefone=" + telefone 
                + ",\n endereco=" + endereco 
                + ",\n data_Cadastro="  +dataC+ '}'
              //  +"\nHashCode: "+hashCode()
                ;
    }

    
   
} 