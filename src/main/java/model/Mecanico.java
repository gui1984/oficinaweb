package model;

import java.io.Serializable;
import java.util.Objects;

/**Essa classe define Mecanico 
 *
 * @author 0230090
 */
public class Mecanico implements Serializable { 
    
    /**
     * atributo da interface serializable
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * Atributos Privados da Classe 
     */
    private long CodMec; 
    private String nomeMec; 

    /**Método contrutor
     *
     * @param nomeMec String
     */
    public Mecanico(String nomeMec) {
        this.nomeMec = nomeMec;
    }

    /**
     *
     * @return long CodMec
     */
    public long getCodMec() {
        return CodMec;
    }

    /**
     *
     * @param CodMec long
     */
    public void setCodMec(long CodMec) {
        this.CodMec = CodMec;
    }

    /**
     *
     * @return nomeMec String
     */
    public String getNomeMec() {
        return nomeMec;
    }
    /**
     * 
     * @param nomeMec  String
     */
    public void setNomeMec(String nomeMec) {
        this.nomeMec = nomeMec;
    }

    /**
     *Hashcode gerado automaticamente
     * @return int
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 13 * hash + Objects.hashCode(this.nomeMec);
        return hash;
    }

    /**
     *Equals gerado automaticamente
     * @param obj Object Mecanico
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Mecanico other = (Mecanico) obj;
        if (!Objects.equals(this.nomeMec, other.nomeMec)) {
            return false;
        }
        return true;
    }

    /**Sobrescreve a classe objeto 
     *toString retornando uma  String com todos os atributos
     *
     * @return String
     */
    @Override
    public String toString() {
        return "Mecanico{" + "\nCodMec=" + CodMec 
                + ", \nnomeMec=" + nomeMec + '}';
    }

    
    
	
}
