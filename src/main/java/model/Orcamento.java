package model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.swing.JOptionPane;

/**
 *Essa classe define o orçamento
 * que possui uma lista de serviços,
 * um mecanico responsavel,
 * um lista de peças(não obrigatorio),
 * um veiculo com cliente imbutido,
 * e o parametro boolean aprovado com false como padrao
 * @author 0230090
 */
public class Orcamento implements Serializable,Comparable<Orcamento> {

     /**
     * atributo da interface serializable
     */
    private static final long serialVersionUID = 1L;
    /**
     * Atributos Privados da Classe 
     */	
    private  long codOrcamento;
    private Date dataEntrada;
    private boolean aprovado;
    private Date dataEntrega;
    private Double valorTotal;
    private Long codVeiculo;
    private Long codMecanico;
    private String defeito;
    /**
     * Set list com a lista de servico presente no orçamento
     * necessario pelo menos um servico prestado
     */
    private Set<Servico> servicoPrestados;
    /**
     * Set list com a lista de peças presente no orçamento
     */
    private Set<Peca> pecasUsadas;// setar quantidade quantidade

    /**
     *Método contrutor 
     * com veiculo e codorcamento
     * @param codVeiculo Veiculo
     * @param codOrcamento Long
     */
    public Orcamento(Long codVeiculo, Long codOrcamento) {
        this.codOrcamento = codOrcamento;
        this.codVeiculo = codVeiculo;

        this.dataEntrada = new Date(System.currentTimeMillis());
        this.aprovado = false;
        this.servicoPrestados = new HashSet<>();
        this.pecasUsadas = new HashSet<>();
        this.valorTotal = 0.0;
        
    }

    /**
     *Método contrutor somento com veiculo
     * @param codVeiculo Veiculo
     */
    public Orcamento(Long codVeiculo) {
         this.codVeiculo = codVeiculo;

        this.dataEntrada = new Date(System.currentTimeMillis());
        this.aprovado = false;
        this.servicoPrestados = new HashSet<>();
        this.pecasUsadas = new HashSet<>();
        this.valorTotal = 0.0;
    }

    
    
    /**
     *
     * @return long codOrcamento
     */
    public long getCodOrcamento() {
        return codOrcamento;
    }

    /**
     *
     * @param codOrcamento long
     */
    public void setCodOrcamento(long codOrcamento) {
        this.codOrcamento = codOrcamento;
    }

    /**
     *
     * @return dataEntrada Date
     */
    public Date getDataEntrada() {
        return dataEntrada;
    }

    /**
     *
     * @param dataEntrada Date
     */
    public void setDataEntrada(Date dataEntrada) {
        this.dataEntrada = dataEntrada;
    }

    /**
     *
     * @return boolean
     */
    public boolean isAprovado() {
        return aprovado;
    }

    /**
     *
     * @param aprovado boolean
     */
    public void setAprovado(boolean aprovado) {
        this.aprovado = aprovado;
    }

    /**
     *
     * @return Date dataEntrega
     */
    public Date getDataEntrega() {
        return dataEntrega;
    }

    /**
     *
     * @param dataEntrega Date
     */
    public void setDataEntrega(Date dataEntrega) {
        this.dataEntrega = dataEntrega;
    }

    /**
     *
     * @return Veiculo
     */
    public Long getCodVeiculo() {
        return codVeiculo;
    }

    /**
     *
     * @param codVeiculo Veiculo
     */
    public void setCodVeiculo(Long codVeiculo) {
        this.codVeiculo = codVeiculo;
    }

    /**
     *
     * @return Mecanico
     */
    public Long getCodMecanico() {
        return codMecanico;
    }

    /**
     *
     * @param codMecanico Mecanico
     */
    public void setCodMecanico(Long codMecanico) {
        this.codMecanico = codMecanico;
    }

    /**
     *
     * @return Set(Servico)
     */
    public Set<Servico> getServicoPrestados() {
        return servicoPrestados;
    }

    /**
     *
     * @param servicoPrestados Set(Servico)
     */
    public void setServicoPrestados(Set<Servico> servicoPrestados) {
        this.servicoPrestados = servicoPrestados;
    }

    /**
     *
     * @return Set(Peca) 
     */
    public Set<Peca> getPecasUsadas() {
        return pecasUsadas;
    }

    /**
     *Adiciona um servico a lista, 
     * se o servico já existir não sera inserido novamente
     * @param servicoPrestados Servico
     */
    public void setServico(Servico servicoPrestados) {
        boolean add = false;
        try {
            add = this.servicoPrestados.add(servicoPrestados);
        } catch (Exception e) {
            //erro
        }
         if (!add) {
            JOptionPane.showMessageDialog(null, " Servico ja presente no Orçamento");
        } else {
           // JOptionPane.showMessageDialog(null, "Servico inserido: " + servicoPrestados.getNomeServico());
            this.valorTotal = valorTotal+  servicoPrestados.getValorServico();
        }
        
    }

    /**Adiciona um peça a lista, 
     * se a peça já existir não sera inserido novamente
     *
     * @param pecasUsadas Peca
     */
    public void setPecas(Peca pecasUsadas) {
        boolean add = false;
        try {
            add = this.pecasUsadas.add(pecasUsadas);
        } catch (Exception e) {
            //erro
            System.out.println("Deu Pau!");
        }
        if (!add) {
            JOptionPane.showMessageDialog(null, " Peças ja presente no Orçamento");
        } else {
           // JOptionPane.showMessageDialog(null, "Peça inserida: " + pecasUsadas.getNomePeca());
            this.valorTotal = valorTotal + (pecasUsadas.getValor()*pecasUsadas.getQuantidade())  ;
        }

    }

    /**
     *
     * @param pecasUsadas Set(Peca)
     */
    public void setPecasUsadas(Set<Peca> pecasUsadas) {
        this.pecasUsadas = pecasUsadas;
    }

    /**
     *Hashcode gerado automaticamente
     * @return int
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 43 * hash + Objects.hashCode(this.dataEntrada);
        hash = 43 * hash + Objects.hashCode(this.codVeiculo);
        return hash;
    }

    /**
     *Equals gerado automaticamente
     * @param obj Object 
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Orcamento other = (Orcamento) obj;
        if (!Objects.equals(this.dataEntrada, other.dataEntrada)) {
            return false;
        }
        if (!Objects.equals(this.codVeiculo, other.codVeiculo)) {
            return false;
        }
        return true;
    }

    /**
     *Sobrescreve a classe objeto 
     *toString retornando uma  String com todos os atributos
     * @return String
     */
    @Override
    public String toString() {
         
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); //formata a data para visualização certa
        String dataC=" ";
        String dataD=" ";
        if (dataEntrega!=null){
         dataC=sdf.format( dataEntrega );
        } 
        if (dataEntrada!=null){
         dataD=sdf.format( dataEntrada );
        }  
        return "\nOrcamento{" 
                + "\ncodOrcamento=" + codOrcamento
                + "\nDefeito: " + defeito
                + ", \ndataEntrada=" + dataD
                
               
                + ", \nservicoPrestados=" + servicoPrestados
                + ",\npecasUsadas=" + pecasUsadas
                
                + ", \ncodVeiculo=" + codVeiculo
                + ", \ncodMecanico=" + codMecanico
                
                + ", \naprovado=" + aprovado 
                + ", \ndataEntrega=" + dataC
                + ", \nvalorTotal=" + valorTotal
                + "\n}"
                ;
    }


    /**
     *Retorna o valor total do orçamento
     * @return Double valorTotal
     */
    
    public Double getValorTotal() {
        return valorTotal;
    }

    /**
     *Adiciona um valor ao orçamento ja existente
     * @param valor Double
     */
    public void setValorTotal(Double valor) {
        this.valorTotal =valorTotal+ valor;
        

    }
    
    public void setValor(Double valor) {
        this.valorTotal = valor;
        

    }

    /**
     *
     * @return StringBuffer  defeito
     */
    public String getDefeito() {
        return defeito;
    }

    /**
     *
     * @param defeito StringBuffer
     */
    public void setDefeito(String defeito) {
        this.defeito = defeito;
    }

    /**
     *Método compareTo para ordenar Orcamento pelo seu codigo
     * @param o Orcamento
     * @return  int
     */
    @Override
    public int compareTo(Orcamento o) {
         if (this.codOrcamento < o.getCodOrcamento() ) {
            return -1;
        }
        if (this.codOrcamento > o.getCodOrcamento()) {
            return 1;
        }
        return 0;
        
      
    }

}
