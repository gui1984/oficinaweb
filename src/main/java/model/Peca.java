package model;

import java.io.Serializable;
import java.util.Objects;

/**
 *Essa classe define as Peças que irao no orcamento
 * @author Guilherme
 */
public class Peca implements Serializable  {
    
     /**
     * atributo da interface serializable
     */
     private static final long serialVersionUID = 1L;
    /**
     * Atributos Da Classe Peças
     */
    private long CodPeca; 
    private String nomePeca;
    private int quantidade=0;//quantidade em estoque
    private double valor;//valor unitario
    
    
    
    /*
     * Metodos construtores da classe
     */
    /**
     *
     * @param nomePeca String
     */
    public Peca(String nomePeca) {
        this.nomePeca = nomePeca;
        adicionarQuantidade();
    }
    
    /**
     *
     * @param nomePeca String
     * @param valor double
     */
    public Peca(String nomePeca, double valor) {
        this.nomePeca = nomePeca;
        this.valor = valor;
        adicionarQuantidade();
    }

    
    /**
     * Métodos Setter e Getter
     */
    /**
     * @return the nomePeca String
     */
    public String getNomePeca() {
        return nomePeca;
    }

    /**
     * @return double valor
     */
    public double getValor() {
        return valor;
    }

    /**
     * @return int quantidade
     */
    public int getQuantidade() {
        return quantidade;
    }

    /**
     * @param nomePeca the nomePeca to set
     */
    public void setNomePeca(String nomePeca) {
        this.nomePeca = nomePeca;
    }

    /**
     * @param valor double  to set
     */
    public void setValor(double valor) {
        this.valor = valor;
    }

    /**
     * @param quantidade int  to set
     */
    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    /**
     * Seta o numero de quantidade para +1
     */
    private void adicionarQuantidade(){
        this.quantidade++;
    }

    public long getCodPeca() {
        return CodPeca;
    }

    public void setCodPeca(long CodPeca) {
        this.CodPeca = CodPeca;
    }

   
    
    /**
     *Hashcode gerado automaticamente
     * @return int
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.nomePeca);
        return hash;
    }

    /**
     *Equals gerado automaticamente
     * @param obj Object Peca
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Peca other = (Peca) obj;
        if (!Objects.equals(this.nomePeca, other.nomePeca)) {
            return false;
        }
        return true;
    }

    

    
     /**
     * Sobrescreve a classe objeto 
     *toString retornando uma  String com todos os atributos
     * @return String
     */
    @Override
    public String toString() {
        return "Peca{" + "CodPeca=" + CodPeca + ", nomePeca=" + nomePeca 
                + ", quantidade=" + quantidade + ", valor=" + valor + '}';
    }
    
}
