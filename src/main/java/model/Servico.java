package model;

import java.io.Serializable;
import java.util.Objects;

/**
 * Essa classe define a lista de Servicos
 *
 * @author 0230090
 */
public class Servico implements Serializable {

    /**
     * atributo da interface serializable
     */
    private static final long serialVersionUID = 1l;
    /**
     * Atributos Privados da Classe
     */
    private long codServico;
    private String nomeServico;
    private String descricao;
    private double valorServico;

    /**
     * Método contrutor para usar em consultas DAO
     *
     * @param nomeServico String
     */
    public Servico(String nomeServico) {
        this.nomeServico = nomeServico;
    }

    /**
     * Método contrutor completo
     *
     * @param nomeServico String
     * @param descricao String
     * @param valorServico double
     */
    public Servico(String nomeServico, String descricao, double valorServico) {
        this.nomeServico = nomeServico;
        this.descricao = descricao;
        this.valorServico = valorServico;
    }

    /**
     * Método contrutor sem descriçao
     *
     * @param nomeServico String
     * @param valorServico double
     */
    public Servico(String nomeServico, double valorServico) {
        this.nomeServico = nomeServico;
        this.descricao = "";
        this.valorServico = valorServico;
    }

    /**
     *
     * @return long codServico
     */
    public long getCodServico() {
        return codServico;
    }

    /**
     *
     * @param codServico long
     */
    public void setCodServico(long codServico) {
        this.codServico = codServico;
    }

    /**
     *
     * @return String nomeServico
     */
    public String getNomeServico() {
        return nomeServico;
    }

    /**
     *
     * @param nomeServico String
     */
    public void setNomeServico(String nomeServico) {
        this.nomeServico = nomeServico;
    }

    /**
     *
     * @return descricao String
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     *
     * @param descricao String
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     *
     * @return valorServico double
     */
    public double getValorServico() {
        return valorServico;
    }

    /**
     *
     * @param valorServico double
     */
    public void setValorServico(double valorServico) {
        this.valorServico = valorServico;
    }

    /**
     * Hashcode gerado automaticamente
     *
     * @return int
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.nomeServico);
        return hash;
    }

    /**
     * Equals gerado automaticamente
     *
     * @param obj Object Serviço
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Servico other = (Servico) obj;
        if (!Objects.equals(this.nomeServico, other.nomeServico)) {
            return false;
        }
        return true;
    }

    

    /**
     * Sobrescreve a classe objeto toString retornando uma String com todos os
     * atributos
     *
     * @return String
     */
    @Override
    public String toString() {
        return "Servico{" + "codServico=" + codServico + ", nomeServico=" 
                + nomeServico + ", descricao=" + descricao 
                + ", valorServico=" + valorServico + '}';
    }
}
