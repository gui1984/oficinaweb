package model;

import java.io.Serializable;
import java.util.Objects;

/**
 * Essa classe define o veiculo, lembrando que cada veiculo possui
 * obrigatoriamente um dono/cliente
 *
 *
 * @author 0230090
 */
public class Veiculo implements Serializable {

    /**
     * atributo da interface serializable
     */
    private static final long serialVersionUID = 7377881373568223393L;
    /**
     * Atributos Privados da Classe
     */
    private long CodVEiculo;
    private String placa;
    private String chassi;
    private String cor;
    private String particularidades;
    private Long idCliente;

    /**
     * Método contrutor
     *
     * @param placa String
     * @param chassi String
     * @param cor String
     * @param particularidades String
     * @param cliente Cliente
     */
    public Veiculo(String placa, String chassi, String cor, String particularidades) {
        this.placa = placa;
        this.chassi = chassi;
        this.cor = cor;
        this.particularidades = particularidades;
        //this.cliente = cliente;
    }

    /**
     * Método contrutor para usar em consultas DAO
     *
     * @param placa String
     */
    public Veiculo(String placa) {
        this.placa = placa;
    }

    /**
     * @return the CodVEiculo long
     */
    public long getCodVEiculo() {
        return CodVEiculo;
    }

    /**
     * @param CodVEiculo long
     */
    public void setCodVEiculo(long CodVEiculo) {
        this.CodVEiculo = CodVEiculo;
    }

    /**
     * @return the placa String
     */
    public String getPlaca() {
        return placa;
    }

    /**
     * @param placa String
     */
    public void setPlaca(String placa) {
        this.placa = placa;
    }

    /**
     * @return cor String
     */
    public String getCor() {
        return cor;
    }

    /**
     * @param cor String
     */
    public void setCor(String cor) {
        this.cor = cor;
    }

    /**
     * @return chassi String
     */
    public String getChassi() {
        return chassi;
    }

    /**
     * @param chassi String
     */
    public void setChassi(String chassi) {
        this.chassi = chassi;
    }

    /**
     * @return the particularidades String
     */
    public String getParticularidades() {
        return particularidades;
    }

    /**
     * @param particularidades String the particularidades to set
     */
    public void setParticularidades(String particularidades) {
        this.particularidades = particularidades;
    }

    /**
     * Hashcode gerado automaticamente
     *
     * @return int
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 19 * hash + Objects.hashCode(this.placa);
        hash = 19 * hash + Objects.hashCode(this.chassi);
        return hash;
    }

    /**
     * Equals gerado automaticamente
     *
     * @param obj Object Veiculo
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Veiculo other = (Veiculo) obj;
        if (!Objects.equals(this.placa, other.placa)) {
            return false;
        }
        if (!Objects.equals(this.chassi, other.chassi)) {
            return false;
        }
        return true;
    }

    public Long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Long idCliente) {
        this.idCliente = idCliente;
    }

    /**
     * toString retornando uma String com todos os atributos
     *
     * @return String
     */
    @Override
    public String toString() {
        return "Veiculo{"
                + ",\nCodVEiculo=" + CodVEiculo
                + "\n Placa=" + placa
                + ",\n chassi=" + chassi
                + ",\n cor=" + cor
                + ",\n particularidades=" + particularidades
                + ",[\n clienteID=" + idCliente
                + "]\n}";
    }

}
