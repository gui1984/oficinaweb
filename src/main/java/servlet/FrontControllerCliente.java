package servlet;

import servlet.cliente.ClienteServlet;
import control.ClienteDAO;
import control.Comando;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Guilherme
 */
@WebServlet(name = "FrontControllerCliente", urlPatterns = {"/cliente/FCCliente"})
public class FrontControllerCliente extends HttpServlet {

    HashMap controle;
    private final String url = "jdbc:oracle:thin:@oracle.inf.poa.ifrs.edu.br:1521:xe";
    private final String master = "0230090";
    private Connection con;
    //private ClienteDAO cdao;

    @Override
    public void init(ServletConfig config) throws ServletException {

        controle = new HashMap();      
        controle.put("ClienteServlet", "servlet.cliente.ClienteServlet");
        controle.put("CadastrarCliente", "servlet.cliente.CadastrarClienteServlet");
        controle.put("ListarCliente", "servlet.cliente.ListarClienteServlet");
        controle.put("EditarCliente", "servlet.cliente.EditarClienteServlet");
        controle.put("DeletarCliente", "servlet.cliente.DeletarClienteServlet");
        controle.put("TestarCliente", "servlet.TestarClienteServlet");
        
        
        try {
            super.init(config);
            Class.forName("oracle.jdbc.driver.OracleDriver");
            //cdao = new ClienteDAO();
            con = DriverManager.getConnection(url, master, master);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ClienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String servletName = request.getParameter("servlet");
        String ClassDestino = (String) controle.get(servletName);

        try {
            Class classe = Class.forName(ClassDestino);
            Comando comando = (Comando) classe.newInstance();
            comando.executar(request, response);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException ex) {
            throw new ServletException(ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        init(this);
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
