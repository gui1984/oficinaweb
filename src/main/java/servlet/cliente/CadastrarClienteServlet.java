package servlet.cliente;

import control.ClienteDAO;
import control.Comando;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Cliente;

/**
 *
 * @author Guilherme
 */
@WebServlet(name = "CadastrarCliente", urlPatterns = {"/CadastrarCliente"})
public class CadastrarClienteServlet extends HttpServlet implements Comando  {

    private final String url = "jdbc:oracle:thin:@oracle.inf.poa.ifrs.edu.br:1521:xe";
    private final String master = "0230090";
    private Connection con;
    private ClienteDAO cdao;

    @Override
    public void init(ServletConfig config) throws ServletException {
        try {
            super.init(config);

            Class.forName("oracle.jdbc.driver.OracleDriver");
            cdao= ClienteDAO.getInstance();
            con = DriverManager.getConnection(url, master, master);

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ClienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String result="erro";
        
        try {
            String vNome = request.getParameter("nome");
            String vRg = request.getParameter("rg");
            String vCpf = request.getParameter("cpf");
            String vTel = request.getParameter("telefone");
            String vEnd = request.getParameter("endereco");
            
            HashMap controle;
            controle = new HashMap();
            controle.put("vNome", vNome);
            controle.put("vRg", vRg);
            controle.put("vCpf", vCpf);
            controle.put("vTel", vTel);
            controle.put("vEnd", vEnd);
            
            Cliente cliente = new Cliente(vNome, vRg, vCpf, vTel);
            if (vEnd != null) {//se o endereço não é nulo
                cliente.setEndereco(vEnd);//adiciona o endereço ao cliente, senão usa o padrao
            }
            
            cdao.criar(con, cliente);
            con.commit();
        } catch (SQLException ex) {
            Logger.getLogger(CadastrarClienteServlet.class.getName()).log(Level.SEVERE, null, ex);
            
        }
        
//          //colocando a string no request
//            request.setAttribute("resultado", clienteEe);
//            
//            response.setContentType("text/html;charset=UTF-8");
//            //enviando o resultado para uma outra pagina
//            request.getRequestDispatcher("resposta.jsp").forward(request, response);
        response.sendRedirect("saida.jsp");

    }

    @Override
    public void executar(HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, SQLException, IOException, ServletException {
        init(this);
        doPost(request, response);
    }

   
}
