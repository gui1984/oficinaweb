package servlet.cliente;

import control.ClienteDAO;
import control.Comando;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Cliente;

/**
 *
 * @author Guilherme
 */
public class ClienteServlet extends HttpServlet implements Comando {

    private final String url = "jdbc:oracle:thin:@oracle.inf.poa.ifrs.edu.br:1521:xe";
    private final String master = "0230090";
    private Connection con;
    private ClienteDAO cdao;

    @Override
    public void init(ServletConfig config) throws ServletException {
        try {
            super.init(config);
            
            Class.forName("oracle.jdbc.driver.OracleDriver");
            cdao= ClienteDAO.getInstance();
            con = DriverManager.getConnection(url, master, master);
           
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ClienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {       

    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                 try {
            String valorDoParametro = request.getParameter("id");
            String edit = request.getParameter("edit");
            
            //Pesquisar Cliente por RG
            String rgE = valorDoParametro;
            Cliente clienteEdit = new Cliente(rgE);//criar um cliente somente com rg para comparar com existente
            
            Cliente clienteEe = (Cliente) cdao.pesquisar(con, clienteEdit); //pega o resultado da pesquisa e transforma em um cliente
            
            if (clienteEe==null){
                clienteEdit = new Cliente("0");
            }
            
            
            //String nome = clienteEe.getNome();
            //colocando a string no request
            request.setAttribute("resultado", clienteEe);
            
            response.setContentType("text/html;charset=UTF-8");
            //enviando o resultado para uma outra pagina
            
            if(edit.equals("true")){
                request.getRequestDispatcher("visualizarcliente.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("editarcliente.jsp").forward(request, response);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ClienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public void executar(HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, SQLException, IOException, ServletException {
        //doGet(request, response);

        init(this);
        processRequest(request, response);

     }

}
