<%-- 
    Document   : listarcliente
    Created on : 29/10/2015, 19:38:44
    Author     : Guilherme
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
         
         <H1> Lista de Pessoas da Família </H1>
     <TABLE BORDER=“1”>
        <TH> Nomes </TH>
        <c:set var="tam" value="${fn:length(resultado)}"  />
        <c:forEach var="i" begin="0" end="${tam}">        
        
              <TR><TD>  <c:out value="${resultado[i]}"/>  </TD></TR>
              
        </c:forEach>
     </TABLE>
    </body>
</html>
